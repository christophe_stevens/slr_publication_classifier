### What for? ###

This application attempts to do automatized systematic academic literature review (SLR). 
The criteria of inclusion are epidemiological studies not on clinical intervention and on human.

This is not to be used alone, as always in SLR an human should select and read the complete reference list. 

The goal here is to make a preselection of articles likely to be excluded and excluded. 

### How do I get set up? ###
In order to run this application you'll need java sdk 1.8 or higher. 

The library used are given in libs folder.

### Input  ###

The input  is a reference file downloaded from Pubmed. 

### Output parameter ###

The output is a RIS file with marked exclusion probability in tag U1 and marked exclusion factor in U2.




