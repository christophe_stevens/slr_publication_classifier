package BL.miner;

/**
 * Created by casteven on 31/05/2016.
 */
public class miningResult {

    private Double probability=0.0;

    public miningResult(Double probability) {
        this.probability = probability;
    }

    public Double getProbability() {
        return probability;
    }

    public String getReason() {
        return (probability>=0.7?("excluded"):((probability>=0.4?(""):("included"))));
    }

}
