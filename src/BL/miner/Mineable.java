package BL.miner;

import BO.Publication.MedLinePublication;
import Util.List_Util;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by casteven on 04/05/2016.
 */
public  class  Mineable extends MedLinePublication
{

    private Map<String,List<String>> cluesInTitle=new HashMap<String,List<String>>();
    private Map<String,List<String>> cluesInAbstract=new HashMap<String,List<String>>();
    private Map<String,List<String>> cluesInMesh=new HashMap<String,List<String>>();

    public Mineable()
    {
        super();
    }

    /**
     * This will mine all the field of the publication for all dictionaries.
     * @param dictionaryNames ( name of the dictionary used, should be be located in /Resource/dbs/* )
     */
    public void mine(List<String> dictionaryNames)
    {
        for(String dictionary:dictionaryNames) {
            this.cluesInAbstract.put( dictionary, mine(super.getAB().toLowerCase(),dictionary));
            this.cluesInTitle.put( dictionary, mine(super.getTI().toLowerCase(), dictionary));
            this.cluesInMesh.put( dictionary, mine(List_Util.convertToCommaSeparatedStringList(super.getMH()).replace(",", " "),dictionary));
        }
    }

    /**
     *
     * @param textToMine is the field currently being mined, (can either be: Mesh terms, Title or  Abstract)
     * @param dictionaryName Dictionary for whish we want to mine the text. (searching for word in dictionary into textToMine.
     * @return the list of String/regex from dictionary that are matched in the textToMine.
     */
    private List<String> mine(String textToMine, String dictionaryName)
    {
        URL dir_url = Mineable.class.getResource("/Resource/dbs/" + dictionaryName);
        List<String> cluesText=new ArrayList<>();

        File dir = null;
        try {
            dir = new File(dir_url.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(dir));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            String line;

            while ((line=br.readLine()) != null) {
                Pattern p = Pattern.compile(line);
                if(p.matcher(textToMine).matches())
                {
                    cluesText.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return cluesText;
    }


    public Map<String, List<String>> getCluesInTitle() {
        return cluesInTitle;
    }

    public Map<String, List<String>> getCluesInAbstract() {
        return cluesInAbstract;
    }

    public Map<String, List<String>> getCluesInMesh() {
        return cluesInMesh;
    }

    /**
     *  This weight the probabilities found depending on their location, evidence found in
     *  Mesh are more significative than evidences found in title, or in abstract.
     *  These are Subjectives values and can be changed according to your own beliefs.
     * @param dictionary ( name of the dictionary used, should be be located in /Resource/dbs/* )
     * @return weighted evidences as Double.
     */
    public Double getOverallClue(String dictionary)
    {
        double[] COEFFICIENTS=new double[]{2.1,2,1.8};
        if (super.getAbstract()==null || super.getAbstract().length()<10)
            COEFFICIENTS=new double[]{3.17,0,2.82};
        else if (List_Util.convertToCommaSeparatedStringList(super.getMH()).replace(",", " ").length()<10)
            COEFFICIENTS=new double[]{0,3.105,2.78};
        return COEFFICIENTS[0]* ((Integer) cluesInMesh.get(dictionary).size()).doubleValue() + COEFFICIENTS[1]* ((Integer) cluesInTitle.get(dictionary).size()).doubleValue() + COEFFICIENTS[2] * ((Integer) cluesInAbstract.get(dictionary).size()).doubleValue() ;
    }

}
