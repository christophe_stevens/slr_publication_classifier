package BL.Writer;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import BL.miner.Mineable;
import BO.Publication.MedLinePublication;
import BO.Publication.Publication;
import BO.Publication.RisPublication;
import Util.FieldParserUtil;
import Util.FileUtil;
import Util.List_Util;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;

public class PublicationExcelFileWriter
{
    private static SXSSFWorkbook wb;
    private static CreationHelper createHelper;

    public static void writeFile(List<Publication> publications, File f)
    {
        CreateWorkbook();
        AddDatasheetPublications(publications);
        saveWorkbook(f);
    }

    public static void writeFileForTree(List<Mineable> publications, File f,List<String> dictionaries)
    {
        CreateWorkbook();
        AddDatasheetCluesInPublications(publications,dictionaries);
        saveWorkbook(f);
    }

    public static void writeMineableFile(List<Mineable> publications, File f)
    {
        CreateWorkbook();
        AddDatasheetPublicationsTree(publications);
        saveWorkbook(f);
    }


    private static void AddDatasheetCluesInPublications(List<Mineable> publications,List<String> dictionaries)
    {
        Sheet sheet = wb.createSheet("Ouput");
        int i=1;

        //Style
        CellStyle Titlestyle = wb.createCellStyle();
        Titlestyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
        Titlestyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        //Font
        Font Totalfont = wb.createFont();
        Totalfont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        //Adding Title
        Row Title_Row = sheet.createRow((short) 0);
        Title_Row.createCell(0).setCellValue(createHelper.createRichTextString("URL"));
        int colIndex=1;
        for (String s:dictionaries)
        {
            Title_Row.createCell(colIndex).setCellValue(s + ".title");colIndex++;
            Title_Row.createCell(colIndex).setCellValue(s + ".abstract");colIndex++;
            Title_Row.createCell(colIndex).setCellValue(s + ".mh");colIndex++;
        }
        Title_Row.createCell(colIndex).setCellValue("inclusion");colIndex++;
        for (Mineable pub:publications)
        {
            Row row = sheet.createRow(i);
            row.createCell(0).setCellValue(createHelper.createRichTextString(pub.getURL()));
            colIndex=1;
            for (String s:dictionaries)
            {
                row.createCell(colIndex).setCellValue(createHelper.createRichTextString(((Integer) pub.getCluesInTitle().get(s).size()).toString())); colIndex++;
                row.createCell(colIndex).setCellValue(createHelper.createRichTextString(((Integer) pub.getCluesInAbstract().get(s).size()).toString())); colIndex++;
                row.createCell(colIndex).setCellValue(createHelper.createRichTextString(((Integer) pub.getCluesInMesh().get(s).size()).toString())); colIndex++;
            }
          /*  String isincluded=pub.getURL();
            boolean included=false;
            List<String> includedBreast=new ArrayList<>( Arrays.asList("18542077","18552361","18555470","18555519","18559548","18584483","18586686","18594543","18596909","18612154","18623079","18628424","18628427","18636564","18647433","18651571","18664548","18665162","18668212","18669582","18669903","18670874","18708394","18711029","18711147","18712981","18712988","18721072","18726992","18756015","18777206","18785003","18791919","18794007","18798278","18826621","18827810","18839430","18842615","18843019","18852405","18855108","18930643","18941914","18976449","18984888","18987981","18990757","18990759","18990767","18990773","18996867","19001601","19004010","19015201","19029398","19029400","19031103","19033562","19035453","19048616","19056569","19056583","19056601","19058195","19064558","19064560","19074205","19077568","19095438","19096929","19116331","19116382","19116874","19123466","19124511","19124518","19150917","19158208","19165862","19184472","19190159","19204221","19211822","19224379","19224978","19244173","19252980","19253081","19255063","19255861","19258487","19273487","19292893","19298602","19318430","19318616","19319984","19330531","19330841","19336565","19346647","19352569","19356229","19358284","19361966","19387852","19389261","19390621","19403632","19403842","19423528","19444907","19452516","19452528","19464165","19469632","19470790","19474140","19480251","19491385","19491389","19491612","19502596","19513076","19533390","19537887","19541857","19548367","19557016","19565333","19566923","19567502","19582883","19584139","19584735","19587089","19593149","19597702","19597749","19619335","19624417","19625685","19626603","19640185","19640186","19640188","19653110","19667298","19667493","19680614","19687338","19690178","19690185","19696953","19697123","19706843","19711189","19715600","19732438","19759169","19760030","19771534","19775332","19789300","19789365","19796379","19828509","19838916","19838918","19843674","19847643","19856117","19859803","19860847","19934322","19936841","19949857","19954572","19960434","19960437","19968892","19996973","20006278","20006279","20007303","20022237","20030812","20039325","20054709","20068186","20084545","20096652","20104980","20104981","20112341","20116235","20130492","20151981","20155314","20160264","20180013","20181808","20197471","20200427","20217489","20305129","20335156","20335555","20338838","20350778","20358467","20364336","20392891","20398298","20410093","20417243","20432163","20432165","20432167","20432168","20440552","20447922","20460303","20473950","20501755","20512657","20541936","20558125","20564638","20565829","20567021","20571498","20574916","20579950","20591221","20592103","20593932","20607384","20615886","20631204","20635135","20647412","20658314","20661665","20680433","20680436","20693310","20698054","20716701","20727220","20728566","20730487","20730623","20733117","20805083","20814736","20826834","20838879","20842123","20843144","20858745","20864719","20878979","20923746","20924962","20935521","20937636","20955599","20959829","20962158","20975025","21029404","21056286","21058205","21064092","21064105","21080051","21084558","21104009","21110223","21127286","21128178","21129090","21143857","21157446","21159569","21160130","21160428","21163903","21198261","21198265","21199800","21207419","21213036","21294673","21295062","21318600","21327461","21335508","21338222","21343248","21353477","21360045","21364029","21403522","21413008","21413010","21415236","21422422","21447479","21466740","21470932","21474525","21537933","21540873","21543628","21551241","21569367","21569535","21587285","21597917","21604157","21606843","21627376","21655715","21667121","21693626","21705842","21710135","21714685","21736840","21767080","21775555","21779757","21782509","21790257","21829196","21834074","21864361","21878422","21880848","21883693","21890443","21934685","21965787","21981584","22020403","22031525","22045766","22056503","22113872","22120517","22126482","22126497","22130867","22132797","22138747","22166249","22194528","22215387","22218885","22279254","22286371","22296351","22318751","22331481","22350922","22362582","22367701","22392404","22405187","22422990","22428342","22443862","22446898","22497978","22502657","22527162","22576580","22583394","22591208","22622862","22623708","22642949","22662004","22675372","22677900","22694827","22726230","22729968","22733561","22755371","22760570","22815728","22821174","22831983","22832206","22860889","22867716","22879204","22886851","22903273","22907507","22938477","22971998","22992276","23011480","23020026","23033240","23074288","23090881","23132534","23137008","23139246","23173778","23179663","23180513","23183091","23185976","23208074","23225141","23239153","23244094","23269820","23319293","23329367","23350064","23354422","23358107","23358902","23389819","23390532","23408942","23421337","23429062","23438695","23441605","23442740","23462913","23478716","23494727","23497298","23497533","23504150","23526070","23526380","23530638","23530639","23532538","23534721","23564813","23571511","23572295","23576427","23592822","23608001","23621274","23628324","23633026","23643804","23649231","23651876","23658396","23669264","23674270","23677576","23682336","23686442","23690776","23697705","23702680","23706176","23713049","23720403","23725163","23726216","23737027","23760859","23762816","23771716","23780838","23785239","23803026","23832257","23856570","23878422","23880155","23880822","23887996","23899816","23901017","23907208","23966890","23985142","23996837","24000037","24008665","24022188","24027626","24037751","24046390","24046808","24054997","24064521","24064741","24066929","24070266","24091793","24091794","24097200","24099317","24108781","24113797","24118876","24127779","24131320","24136668","24153343","24155133","24158779","24166746","24175799","24175800","24179896","24204664","24206792","24223241","24231452","24244548","24249438","24252436","24274352","24281852","24289300","24289609","24290080","24321460","24337883","24349006","24369207","24374950","24377643","24403289","24410858","24423614","24438060","24443815","24455581","24500704","24502868","24504557","24512931","24517946","24519551","24548731","24548865","24559007","24569437","24574508","24580799","24582912","24586662","24587366","24596595","24599882","24606431","24606455","24613622","24625472","24629213","24633137","24636229","24639841","24667649","24675385","24682675","24691133","24692081","24692102","24708573","24714744","24715379","24715420","24716978","24718872","24723234","24737167","24743350","24748579","24752603","24761911","24769568","24771551","24771654","24777595","24781974","24789344","24789514","24789846","24801368","24810653","24817834","24829113","24831616","24847911","24853250","24859854","24870727","24871563","24892153","24894342","24916719","24920034","24924530","24932496","24934582","24935969","24941118","24941967","24955316","24963789","24969864","24985407","24986698","24993766","24994839","24997743","25013701","25015335","25049109","25072980","25077380","25078601","25081729","25096278","25097392","25101568","25103823","25114017","25129328","25135447","25143370","25150269","25153314","25156617","25160079","25184210","25198347","25198723","25201305","25207209","25217320","25220168","25228650","25242052","25252818","25258014","25265504","25270516","25283589","25284450","25292081","25307804","25320621","25332681","25335465","25339024","25339056","25351244","25352523","25352526","25366783","25370683","25379993","25403648","25421379","25422252","25422253","25422909","25430815","25442219","25460125","25504309","25505228","25520082","25543181","25559553","25587642","25604861","25624428","25637171","25677034","25692500","25693843","25703993","25740737","25746827","25747120","25757865","25771535","25783428","25783459","25784976","25785349","25785639","25796612","25804877","25805068","25805146","25817193"));
            for(String s:includedBreast)
                      if(isincluded.equals("PM:" + s))
                          included=true;
            row.createCell(colIndex).setCellValue(createHelper.createRichTextString(((Boolean)included).toString()));  */
            i++;
        }
    }

    private static void CreateWorkbook()
    {
        wb = new SXSSFWorkbook(100);
        wb.setCompressTempFiles(true);
        createHelper = wb.getCreationHelper();
    }

    private static void AddDatasheetPublications(List<Publication> publications)
    {
        Sheet sheet = wb.createSheet("Ouput");
        int i=1;

            //Style
            CellStyle Titlestyle = wb.createCellStyle();
            Titlestyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            Titlestyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            //Font
            Font Totalfont = wb.createFont();
            Totalfont.setBoldweight(Font.BOLDWEIGHT_BOLD);
            //Adding Title
            Row Title_Row = sheet.createRow((short) 0);
            Title_Row.createCell(0).setCellValue(createHelper.createRichTextString("U3"));
            Title_Row.createCell(1).setCellValue(createHelper.createRichTextString("U4"));
            Title_Row.createCell(2).setCellValue(createHelper.createRichTextString("ID"));
            Title_Row.createCell(3).setCellValue(createHelper.createRichTextString("T1"));
            Title_Row.createCell(4).setCellValue(createHelper.createRichTextString("A1..A1"));
            Title_Row.createCell(5).setCellValue(createHelper.createRichTextString("Y1"));
            Title_Row.createCell(6).setCellValue(createHelper.createRichTextString("N2"));
            Title_Row.createCell(7).setCellValue(createHelper.createRichTextString("KW..KW"));
            Title_Row.createCell(8).setCellValue(createHelper.createRichTextString("JF"));
            Title_Row.createCell(9).setCellValue(createHelper.createRichTextString("JA"));
            Title_Row.createCell(10).setCellValue(createHelper.createRichTextString("VL"));
            Title_Row.createCell(11).setCellValue(createHelper.createRichTextString("IS"));//Issue Number
            Title_Row.createCell(12).setCellValue(createHelper.createRichTextString("SP"));
            Title_Row.createCell(13).setCellValue(createHelper.createRichTextString("EP"));
            Title_Row.createCell(14).setCellValue(createHelper.createRichTextString("CY"));
            Title_Row.createCell(15).setCellValue(createHelper.createRichTextString("PB"));
            Title_Row.createCell(16).setCellValue(createHelper.createRichTextString("SN"));//ISSN or ISBN
            Title_Row.createCell(17).setCellValue(createHelper.createRichTextString("M1"));
            Title_Row.createCell(18).setCellValue(createHelper.createRichTextString("U1"));
            Title_Row.createCell(19).setCellValue(createHelper.createRichTextString("U2"));
            Title_Row.createCell(20).setCellValue(createHelper.createRichTextString("U5"));
            Title_Row.createCell(21).setCellValue(createHelper.createRichTextString("URL"));
            Title_Row.createCell(22).setCellValue(createHelper.createRichTextString("File"));
            for (int index=0;index<=22;index++)
                Title_Row.getCell(index).setCellStyle(Titlestyle);
            for (Publication pub:publications)
            {
                Row row = sheet.createRow(i);
                if (pub.getClass().equals(MedLinePublication.class))
                {
                    createMedLineRow((MedLinePublication)pub,row);
                }
                else if (pub.getClass().equals(RisPublication.class))
                {
                    createRisRow((RisPublication)pub,row);
                }
                i++;
            }
    }


    private static void AddDatasheetPublicationsTree(List<Mineable> publications)
    {
        Sheet sheet = wb.createSheet("Ouput");
        int i=1;

        //Style
        CellStyle Titlestyle = wb.createCellStyle();
        Titlestyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
        Titlestyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        //Font
        Font Totalfont = wb.createFont();
        Totalfont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        //Adding Title
        Row Title_Row = sheet.createRow((short) 0);
        Title_Row.createCell(0).setCellValue(createHelper.createRichTextString("PMID"));
        Title_Row.createCell(1).setCellValue(createHelper.createRichTextString("Exclusion"));
        for (int index=0;index<=1;index++)
            Title_Row.getCell(index).setCellStyle(Titlestyle);
        for (Mineable pub:publications)
        {
            Row row = sheet.createRow(i);
            row.createCell(0).setCellValue(createHelper.createRichTextString(pub.getPMID()));
            row.createCell(1).setCellValue(createHelper.createRichTextString(pub.getUserDefined1()));
            i++;
        }
    }

    private static void createMedLineRow(MedLinePublication p, Row r)
    {
        r.createCell(0).setCellValue(createHelper.createRichTextString(p.getUserDefined3()==null?"":p.getUserDefined3()));
        r.createCell(1).setCellValue(createHelper.createRichTextString(p.getUserDefined4()==null?"":p.getUserDefined4()));
        r.createCell(2).setCellValue(createHelper.createRichTextString(p.getPMID()==null?"":p.getPMID()));
        r.createCell(3).setCellValue(createHelper.createRichTextString(p.getTI()==null?"":p.getTI()));
        r.createCell(4).setCellValue(createHelper.createRichTextString(p.getFAU().size()==0?"":Util.List_Util.convertToCommaSeparatedStringList(p.getFAU())));
        r.createCell(5).setCellValue(createHelper.createRichTextString(p.getDP()==null?"":p.getDP()));
        r.createCell(6).setCellValue(createHelper.createRichTextString(p.getAB()==null?"":p.getAB()));
        r.createCell(7).setCellValue(createHelper.createRichTextString(p.getMH().size()==0?"":Util.List_Util.convertToCommaSeparatedStringList(p.getMH())));
        r.createCell(8).setCellValue(createHelper.createRichTextString(p.getJT()==null?"":p.getJT()));
        r.createCell(9).setCellValue(createHelper.createRichTextString(p.getTA()==null?"":p.getTA()));
        r.createCell(10).setCellValue(createHelper.createRichTextString(p.getVI()==null?"":p.getVI()));
        r.createCell(11).setCellValue(createHelper.createRichTextString(p.getIP()==null?"":p.getIP())); //Issue Number
        r.createCell(12).setCellValue(createHelper.createRichTextString(p.getPG()==null?"":p.getStartPageNumber()));
        r.createCell(13).setCellValue(createHelper.createRichTextString(p.getPG()==null?"":p.getEndPageNumber()));
        r.createCell(14).setCellValue(createHelper.createRichTextString(p.getPL()==null?"":p.getPL()));
        r.createCell(15).setCellValue(createHelper.createRichTextString(p.getAD()==null?"":p.getAD()));
        r.createCell(16).setCellValue(createHelper.createRichTextString((p.getISSN().size()==0?"": List_Util.convertToCommaSeparatedStringList(p.getISSN()) +  (p.getISBN()==null?"\\":p.getISBN())))); // ISBN Or ISSN
        r.createCell(17).setCellValue(createHelper.createRichTextString(p.getJID().size()==0?"":Util.List_Util.convertToCommaSeparatedStringList(p.getJID())));
        r.createCell(18).setCellValue(createHelper.createRichTextString(p.getUserDefined1()==null?"":p.getUserDefined1()));//U1  not existing in MedLine
        r.createCell(19).setCellValue(createHelper.createRichTextString(p.getUserDefined2()==null?"":p.getUserDefined2()));//U2  not Existing in MedLine
        r.createCell(20).setCellValue(createHelper.createRichTextString(""));//U5  not Existing in MedLine
        r.createCell(21).setCellValue(createHelper.createRichTextString(p.getURL()==null?"":p.getURL()));//UR  not Existing in MedLine
        r.createCell(22).setCellValue(createHelper.createRichTextString(p.getPubFile().getName()));
    }
    private static void createRisRow(RisPublication p, Row r)
    {

        r.createCell(0).setCellValue(createHelper.createRichTextString(p.getUserDefined3()==null?"":p.getUserDefined3()));
        r.createCell(1).setCellValue(createHelper.createRichTextString(p.getUserDefined4()==null?"":p.getUserDefined4()));
        r.createCell(2).setCellValue(createHelper.createRichTextString(p.getID()==null?"":p.getID()));
        r.createCell(3).setCellValue(createHelper.createRichTextString(p.getT1()==null?"":p.getT1()));
        r.createCell(4).setCellValue(createHelper.createRichTextString(p.getA1().size()==0?"":Util.List_Util.convertToCommaSeparatedStringList(p.getA1())));
        r.createCell(5).setCellValue(createHelper.createRichTextString(p.getY1()==null?"":p.getY1()));
        r.createCell(6).setCellValue(createHelper.createRichTextString(p.getN2()==null?"":p.getN2()));
        r.createCell(7).setCellValue(createHelper.createRichTextString(p.getKW().size()==0?"":Util.List_Util.convertToCommaSeparatedStringList(p.getKW())));
        r.createCell(8).setCellValue(createHelper.createRichTextString(p.getJF()==null?"":p.getJF()));
        r.createCell(9).setCellValue(createHelper.createRichTextString(p.getJA()==null?"":p.getJA()));
        r.createCell(10).setCellValue(createHelper.createRichTextString(p.getVL()==null?"":p.getVL()));
        r.createCell(11).setCellValue(createHelper.createRichTextString(p.getIS()==null?"" :p.getIS()));
        r.createCell(12).setCellValue(createHelper.createRichTextString(p.getSP()==null?"":p.getSP()));
        r.createCell(13).setCellValue(createHelper.createRichTextString(p.getEP()==null?"":p.getEP()));
        r.createCell(14).setCellValue(createHelper.createRichTextString(p.getCY()==null?"":p.getCY()));
        r.createCell(15).setCellValue(createHelper.createRichTextString(p.getPB()==null?"":p.getPB()));
        r.createCell(16).setCellValue(createHelper.createRichTextString(p.getSN()==null?"":p.getSN()));
        r.createCell(17).setCellValue(createHelper.createRichTextString(p.getM1().size()==0?"":Util.List_Util.convertToCommaSeparatedStringList(p.getM1())));
        r.createCell(18).setCellValue(createHelper.createRichTextString(p.getU1()==null?"":p.getU1()));
        r.createCell(19).setCellValue(createHelper.createRichTextString(p.getU2()==null?"":p.getU2()));
        r.createCell(20).setCellValue(createHelper.createRichTextString(p.getU5()==null?"":p.getU5()));
        r.createCell(21).setCellValue(createHelper.createRichTextString(p.getUR()==null?"":p.getUR()));
        r.createCell(22).setCellValue(createHelper.createRichTextString(p.getPubFile().getName()));
    }

    private static void saveWorkbook(File f)
    {
        try
        {
            FileOutputStream fout;
            fout = new FileOutputStream(f);
            wb.write(fout);
            fout.close();
        }
        catch (FileNotFoundException exception1)
        {

        }catch (IOException ioErr)
        {
        }
        FileUtil.OpenFile(f.getAbsolutePath());
    }
}
