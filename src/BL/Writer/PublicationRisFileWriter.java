package BL.Writer;

import BL.classifier.Classifier;
import BL.miner.Mineable;
import BL.classifier.Tree;
import BL.miner.miningResult;
import BO.Publication.MedLinePublication;
import BO.Publication.Publication;
import BO.Publication.RisPublication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by casteven on 18/02/14.
 */
public class PublicationRisFileWriter
{
    public static void writeFile(List<Mineable> publications, File DestinationFile, Classifier.Classifiers cls)
{
    PrintWriter writer = null;
    try {
        writer = new PrintWriter(DestinationFile);
        for  (Publication pu:publications)
        {
            if (pu.getClass().equals(Mineable.class))
                writeMineablePublication((Mineable) pu, writer,cls);
            writer.print(buildElement("ER",""));
            writer.print("\r\n");
        }
        writer.close();
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    }
}

    private static void writeMineablePublication(Mineable p, PrintWriter out, Classifier.Classifiers cls)
    {
        miningResult mr= Classifier.getClassify(cls,p);
        out.print(buildElement("TY","JOUR"));
        //if (p.getPMID()!=null) out.print(buildElement("ID",p.getPMID()));
        if (p.getTI()!=null) out.print(buildElement("T1",p.getTI()));
        if (p.getFAU().size()>0)
            for (String s:p.getFAU())
                out.print(buildElement("A1",s));
        if (p.getDP()!=null) out.print(buildElement("Y1",p.getDP()));
        if (p.getAB()!=null) out.print(buildElement("N2",p.getAB()));
        if (p.getMH().size()>0)
            for (String s:p.getMH())
                out.print(buildElement("KW",s));
        if (p.getJT()!=null) out.print(buildElement("JF",p.getJT()));
        if (p.getTA()!=null) out.print(buildElement("JA",p.getTA()));
        if (p.getVI()!=null) out.print(buildElement("VL",p.getVI()));
        if (p.getIP()!=null) out.print(buildElement("IS",p.getIP()));
        String NotesValue="";
        if (p.getPMID()!=null)
            NotesValue+="PMID:" + p.getPMID() + ";";
        if (p.getISSN().size()>0)
            for (String s:p.getISSN())
                NotesValue+="ISSN:" + s + ";";
        if (p.getISBN()!=null) NotesValue+="ISBN:" + p.getISBN() + ";";
        if (p.getPT().size()>0)
            for (String s:p.getPT())
                NotesValue+="PT:" + s + ";";
        if (p.getLA()!=null) NotesValue+="LA:" + p.getLA() + ";";
        if (!NotesValue.isEmpty()) out.print(buildElement("N1",NotesValue));
        if (p.getPMID()!=null) out.print(buildElement("UR","PM:" + p.getPMID()));
        if (p.getPG()!=null) out.print(buildElement("SP",p.getStartPageNumber()));
        if (p.getPG()!=null) out.print(buildElement("EP",p.getEndPageNumber()));
        if (p.getPL()!=null) out.print(buildElement("CY",p.getPL()));
        if (p.getAD()!=null) out.print(buildElement("PB",p.getAD()));
        if (p.getDA()!=null) out.print(buildElement("PY",p.getDA()));
        if (p.getAID()!=null) out.print(buildElement("DO",p.getAID()));
        if (p.getUserDefined1()!=null) out.print(buildElement("U1", mr.getProbability().toString()));
        if (p.getUserDefined2()!=null) out.print(buildElement("U2",mr.getReason()));
        if (p.getUserDefined3()!=null) out.print(buildElement("U3",p.getUserDefined3()));
        if (p.getUserDefined4()!=null) out.print(buildElement("U4",p.getUserDefined4()));
        if (p.getUserDefined4()!=null) out.print(buildElement("UR",p.getURL()));
    }
    private static String buildElement(String tag, String Value)
    {
        String Line=tag;
        int spaces=4-tag.length();
        for (int i = 0; i < spaces; i++)
            Line+=" ";
        Line+="- ";
        Line+=Value;
        Line+="\r\n";
        return Line;
    }



}
