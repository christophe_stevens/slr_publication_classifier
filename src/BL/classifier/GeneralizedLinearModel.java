package BL.classifier;

import BL.miner.Mineable;
import BL.miner.miningResult;

/**
 * Created by casteven on 15/06/2016.
 */
public class GeneralizedLinearModel
{
    /**
     *
     * @param mined  (publication that has been minded)
     * @return  probability of exclusion based on a genralized linear model.
     */
    protected static Double exclusionProbability(Mineable mined)
    {
        Double predictorValue= -3.660
                +0.886*mined.getOverallClue("cancer_epidemiological")-0.09325*Math.pow(mined.getOverallClue("cancer_epidemiological"),2)+0.004345*Math.pow(mined.getOverallClue("cancer_epidemiological"),3)-0.00006923*Math.pow(mined.getOverallClue("cancer_epidemiological"),4)
                +1.877*mined.getOverallClue("cancer_nonepidemiological")-0.5624*Math.pow(mined.getOverallClue("cancer_nonepidemiological"),2)+0.05907*Math.pow(mined.getOverallClue("cancer_nonepidemiological"),3)
                -0.4525*mined.getOverallClue("cancer_human")+0.04355*Math.pow(mined.getOverallClue("cancer_human"),2)-0.0009852*Math.pow(mined.getOverallClue("cancer_human"),3)
                -0.8125*mined.getOverallClue("cancer_clinical");
        Double predictedValue= Math.pow(Math.exp(1),predictorValue)/(1+Math.pow(Math.exp(1),predictorValue));
        return (1.0-predictedValue);
    }

}
