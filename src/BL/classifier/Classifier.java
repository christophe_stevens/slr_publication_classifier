package BL.classifier;

import BL.miner.Mineable;
import BL.miner.miningResult;

/**
 * Created by casteven on 15/06/2016.
 */
public class Classifier
{
    public enum Classifiers{TREE,GLM,BAYES,COMBINED}

    /**
     *
     * @param cls is the type of Classifier used Tree, GLM, BAYES or COMBINED
     * @param mined  is the mined the article. (on which .mine() method has been executed
     * @return A mining Result containing a qualitative exclsion factor and and quantitative exclusion number.
     */
    public static miningResult getClassify(Classifiers cls, Mineable mined)
    {
          if (cls.equals(Classifiers.GLM))
              return getClassifiedByGLM(mined);
          else if (cls.equals(Classifiers.TREE))
              return  getClassifiedByTree(mined);
          else if (cls.equals(Classifiers.BAYES))
              return  getClassifiedByBayes(mined);
          else
              return  getClassifiedByAll(mined);
    }


    private static miningResult getClassifiedByTree(Mineable mined)
    {
        Double ptree= Tree.exclusionProbability(mined);
        miningResult averaged=new miningResult( ptree);
        return averaged;
    }

    private static miningResult getClassifiedByGLM(Mineable mined)
    {
        Double pglm= GeneralizedLinearModel.exclusionProbability(mined);
        miningResult averaged=new miningResult( pglm);
        return averaged;
    }

    private static miningResult getClassifiedByBayes(Mineable mined)
    {
        Double pbayes= NaiveBayes.exclusionProbability(mined);
        miningResult averaged=new miningResult(pbayes);
        return averaged;
    }

    /**
     * When classified by All algorithm, we try to minimise the false positive (when an article is excluded but it shouldn't be)
     * As this error has worst consequences on SLR process.
     * For that we use the minimum and maximum values to decide the exclusion.
     * @param mined
     * @return A mining Result containing a qualitative exclsion factor and and quantitative exclusion number.
     */
    private static miningResult getClassifiedByAll(Mineable mined)
    {
        Double pglm= GeneralizedLinearModel.exclusionProbability(mined);
        Double ptree= Tree.exclusionProbability(mined);
        Double pbayes= NaiveBayes.exclusionProbability(mined);

        if (pglm>=0.7 & ptree>=0.7 & pbayes>=0.7 )
              return new miningResult(Math.min(Math.min(pglm,ptree),pbayes));
        else if (pglm<0.3 & ptree<0.3 & pbayes<0.3 )
              return new miningResult(Math.max(Math.max(pglm,ptree),pbayes));
        else  if ((pglm>0.3 | ptree>0.3 | pbayes>0.3) & (pglm<0.7 & ptree<0.7 & pbayes<0.7))
              return new miningResult(Math.max(Math.max(pglm,pbayes),ptree));
        else if ((pglm<0.7 | ptree<0.7 | pbayes<0.7 ) & (pglm>=0.3 & ptree>=0.3 & pbayes>0.3))
             return new miningResult(Math.min(Math.min(pglm, pbayes), ptree));
        else
            return new miningResult((pglm+ptree+pbayes)/3);
    }
}
