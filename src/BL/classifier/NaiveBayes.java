package BL.classifier;

import BL.miner.Mineable;
import org.apache.commons.math3.distribution.NormalDistribution;


/**
 * Created by casteven on 16/06/2016.
 */
public class NaiveBayes
{
    /**
     *
     * @param mined  (publication that has been minded)
     * @return  probability of exclusion based on a NaiveBayes classifier
     */
    protected static Double exclusionProbability(Mineable mined)
    {
        NormalDistribution epidemiologyIncludedDistribution=new NormalDistribution(7.3782361,6.184186);
        NormalDistribution nonEpidemiologyIncludedDistribution=new NormalDistribution(1.8813656,2.0567815);
        NormalDistribution clinicalIncludedDistribution=new NormalDistribution(0.1079659,0.482508);
        NormalDistribution humanIncludedDistribution=new NormalDistribution(16.37767,4.341593);

        NormalDistribution epidemiologyExcludedDistribution=new NormalDistribution(0.9646614,2.676313);
        NormalDistribution nonEpidemiologyExcludedDistribution=new NormalDistribution(0.1409871,0.6731636);
        NormalDistribution clinicalExcludedDistribution=new NormalDistribution(1.2383951,2.084492);
        NormalDistribution humanExcludedDistribution=new NormalDistribution(10.87870,4.987066);

        double priorIncluded=0.07056816;
        double priorExcluded=0.92943184;

        double posteriorIncluded= priorIncluded
                *epidemiologyIncludedDistribution.density(mined.getOverallClue("cancer_epidemiological"))
                *nonEpidemiologyIncludedDistribution.density(mined.getOverallClue("cancer_nonepidemiological"))
                *clinicalIncludedDistribution.density(mined.getOverallClue("cancer_clinical"))
                *humanIncludedDistribution.density(mined.getOverallClue("cancer_human"));

        double posteriorExcluded= priorExcluded
                *epidemiologyExcludedDistribution.density(mined.getOverallClue("cancer_epidemiological"))
                *nonEpidemiologyExcludedDistribution.density(mined.getOverallClue("cancer_nonepidemiological"))
                *clinicalExcludedDistribution.density(mined.getOverallClue("cancer_clinical"))
                *humanExcludedDistribution.density(mined.getOverallClue("cancer_human"));

        Double predictedExclusionValue=posteriorExcluded/(posteriorExcluded+ posteriorIncluded);

        return (predictedExclusionValue);
    }
}
