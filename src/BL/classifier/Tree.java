package BL.classifier;

import BL.miner.Mineable;
import BL.miner.miningResult;

/**
 * Created by casteven on 31/05/2016.
 */
public class Tree
{
    /**
     *
     * @param mined  (publication that has been minded)
     * @return  probability of exclusion based on a classification Tree.
     */
    protected static Double exclusionProbability(Mineable mined)
    {
        if (mined.getOverallClue("cancer_epidemiological")<0.9)
            return 0.98;
        else
            if(mined.getOverallClue("cancer_nonepidemiological")<0.9)
                if (mined.getOverallClue("cancer_epidemiological")<4)
                    return  0.92;
                else
                   if (mined.getOverallClue("cancer_clinical")>=2.8)
                       return 0.99;
                   else
                       if (mined.getOverallClue("cancer_human")<8.2)
                           return  0.75;
                       else
                            if (mined.getOverallClue("cancer_epidemiological")<6.1)
                                if (mined.getOverallClue("cancer_human")<14)
                                    if (mined.getOverallClue("cancer_epidemiological")<5.9)
                                        return  0.61;
                                    else
                                        if (mined.getOverallClue("cancer_human")>=10)
                                        return  0.64;
                                    else
                                        return  0.30;
                                else
                                    return  0.34;
                            else
                                return  0.30;
            else
                if(mined.getOverallClue("cancer_nonepidemiological")<4)
                    if(mined.getOverallClue("cancer_human")<9.7)
                        return  0.66;
                    else
                        if(mined.getOverallClue("cancer_clinical")>=0.9)
                            return  0.68;
                        else
                            if (mined.getOverallClue("cancer_epidemiological")>=4.8)
                                return  0.60;
                            else
                                return  0.34;
                else
                    return  0.19;

    }

}
