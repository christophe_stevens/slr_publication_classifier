package BL.Parser;

import BO.Files.MedLinePublicationFile;
import BO.Journal.MedLineJournal;
import com.sun.org.apache.bcel.internal.generic.NEWARRAY;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by casteven on 26/02/14.
 */
public class MedLineJournalsParser implements Runnable
{
    private static final String NEW_JOURNAL_TAG="--------------------------------------------------------";
    private static String [] labels=new String[]{"JrId","JournalTitle","MedAbbr","ISSN (Print)","ISSN (Online)", "IsoAbbr", "NlmId"};

    public static List<MedLineJournal> Stat_journals=new ArrayList<MedLineJournal>();

    @Override
    public void run() {
        URL dir_url = MedLineJournal.class.getResource("/Resource/dbs/J_Medline");
        File dir = null;
        try {
            dir = new File(dir_url.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(dir));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<MedLineJournal> journals= new ArrayList<MedLineJournal>();
        MedLineJournal currentJournal=null;
        try {
            String line;
            while ((line=br.readLine()) != null) {
                if (isNewJournal(line))
                {
                   if (currentJournal!=null)
                       journals.add(currentJournal);
                    currentJournal=new MedLineJournal();
                }
                else if (isNewTag(line))
                {
                    if (line.startsWith("JrId"))
                        currentJournal.setJrId(line.split(":")[1].isEmpty()?"":line.split(":")[1].trim());
                    if (line.startsWith("JournalTitle"))
                        currentJournal.setJournalTitle(line.split(":")[1].isEmpty()?"":line.split(":")[1].trim());
                    if (line.startsWith("MedAbbr"))
                        currentJournal.setMedAbbr(line.split(":")[1].isEmpty()?"":line.split(":")[1].trim());
                    if (line.startsWith("ISSN (Print)"))
                        currentJournal.setISSNp(line.split(":")[1].isEmpty()?"":line.split(":")[1].trim());
                    if (line.startsWith("ISSN (Online)"))
                        currentJournal.setISSNo(line.split(":")[1].isEmpty()?"":line.split(":")[1].trim());
                    if (line.startsWith("IsoAbbr"))
                        currentJournal.setIsoAbbr(line.split(":")[1].isEmpty()?"":line.split(":")[1].trim());
                    if (line.startsWith("NlmId"))
                        currentJournal.setNlmId(line.split(":")[1].isEmpty()?"":line.split(":")[1].trim());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            journals.add(currentJournal);
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Stat_journals=journals;
    }

    private static boolean isNewTag(String Line)
    {
       for (int i=0; i<labels.length;i++)
        if (Line.startsWith(labels[i]) )
            return true;
       return false;
    }

    private static boolean isNewJournal(String Line)
    {
        return Line.equals(NEW_JOURNAL_TAG);
    }

    public static String findFullJournalName(String abbrev)
    {
        if (abbrev.endsWith("."))
            abbrev=abbrev.substring(0,abbrev.lastIndexOf("."));
         for (MedLineJournal mJ:Stat_journals)
         {
             if (mJ.getMedAbbr().toUpperCase().equals(abbrev.toUpperCase()))
                 return mJ.getJournalTitle();
         }
        return null;
    }


}
