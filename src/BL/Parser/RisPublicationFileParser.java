package BL.Parser;

import BO.DatabaseType;
import BO.Files.RisPublicationFile;
import BO.Publication.MedLinePublication;
import BO.Publication.RisPublication;
import BO.SearchEngineType;
import Util.FieldParserUtil;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by casteven on 17/02/14.
 */
public class RisPublicationFileParser
{

    private static String[] tags=new String[]{"A1","AD","CY","EP","ER","ID","IS","JA","JF","KW","L1",
                                                "L2","L3","L4","M1","M3","N2","PB","SN","SP","T1","TI",
                                                "TY","UR","VL","Y1","AU","A2","PY","CT","N1","RP","JO",
                                                "J1","J2","T2","T3","AV","U1","U2","U3","U4","U5"};



    public static RisPublicationFile ParseFile(RisPublicationFile file)
    {
        List<RisPublication> publications=new ArrayList<RisPublication>();
        try {
            for (List<PublicationElement> publicationElementList:extractPublicationElememts(file))
            {
                RisPublication publication=new RisPublication();
                for (PublicationElement element:publicationElementList)
                {
                    if (element.Tag!=null)
                    {
                        if (element.Tag.equals("A1"))
                            publication.addA1(element.Value);
                        if (element.Tag.equals("AD"))
                            publication.setAD(element.Value);
                        if (element.Tag.equals("CY"))
                            publication.setCY(element.Value);
                        if (element.Tag.equals("EP"))
                            publication.setEP(element.Value);
                        if (element.Tag.equals("ID"))
                            publication.setID(element.Value);
                        if (element.Tag.equals("IS"))
                            publication.setIS(element.Value);
                        if (element.Tag.equals("JA"))
                            publication.setJA(element.Value);
                        if (element.Tag.equals("JF"))
                            publication.setJF(element.Value);
                        if (element.Tag.equals("KW"))
                            publication.addKW(element.Value);
                        if (element.Tag.equals("L1"))
                            publication.setL1(element.Value);
                        if (element.Tag.equals("L2"))
                            publication.setL2(element.Value);
                        if (element.Tag.equals("L3"))
                            publication.setL3(element.Value);
                        if (element.Tag.equals("L4"))
                            publication.setL4(element.Value);
                        if (element.Tag.equals("M1"))
                            publication.setM1(element.Value);
                        if (element.Tag.equals("M3"))
                            publication.setM3(element.Value);
                        if (element.Tag.equals("N2"))
                            publication.setN2(element.Value);
                        if (element.Tag.equals("PB"))
                            publication.setPB(element.Value);
                        if (element.Tag.equals("SN"))
                            publication.setSN(element.Value);
                        if (element.Tag.equals("SP"))
                            publication.setSP(element.Value);
                        if (element.Tag.equals("T1"))
                            publication.setT1(element.Value);
                        if (element.Tag.equals("TI"))
                            publication.setTI(element.Value);
                        if (element.Tag.equals("TY"))
                            publication.setTY(element.Value);
                        if (element.Tag.equals("UR"))
                            publication.setUR(element.Value);
                        if (element.Tag.equals("VL"))
                            publication.setVL(element.Value);
                        if (element.Tag.equals("Y1"))
                            publication.setY1(element.Value);
                        if (element.Tag.equals("AU"))
                            publication.setAU(element.Value);
                        if (element.Tag.equals("A2"))
                            publication.setA2(element.Value);
                        if (element.Tag.equals("PY"))
                            publication.setPY(element.Value);
                        if (element.Tag.equals("CT"))
                            publication.setCT(element.Value);
                        if (element.Tag.equals("N1"))
                            publication.setN1(element.Value);
                        if (element.Tag.equals("RP"))
                            publication.setRP(element.Value);
                        if (element.Tag.equals("JO"))
                            publication.setJO(element.Value);
                        if (element.Tag.equals("J1"))
                            publication.setJ1(element.Value);
                        if (element.Tag.equals("J2"))
                            publication.setJ2(element.Value);
                        if (element.Tag.equals("T2"))
                            publication.setT2(element.Value);
                        if (element.Tag.equals("T3"))
                            publication.setT3(element.Value);
                        if (element.Tag.equals("AV"))
                            publication.setAV(element.Value);
                        if (element.Tag.equals("U1"))
                            publication.setU1(element.Value);
                        if (element.Tag.equals("U2"))
                            publication.setU2(element.Value);
                        if (element.Tag.equals("U3"))
                            publication.setU3(element.Value);
                        if (element.Tag.equals("U4"))
                            publication.setU4(element.Value);
                        if (element.Tag.equals("U5"))
                            publication.setU5(element.Value);
                        if (element.Tag.equals("DO"))
                            publication.setDO(element.Value);
                    }
                }
                //If MedLine from OviSp, build second page on standard page format
                if (file.getSearchEngineType().equals(SearchEngineType.OvidSp) && file.getDatabaseType().equals(DatabaseType.MEDLINE))
                {
                    if (publication.getEP()==null)
                    {
                        String [] pages= Util.FieldParserUtil.ParsePageField(publication.getSP());
                        publication.setSP(pages[0]);
                        publication.setEP(pages[1]);
                    }
                    publication.setEP(Util.FieldParserUtil.BuildSecondPageOnMedLineNotation(publication.getSP(), publication.getEP()));
                }
                publication.setSP(FieldParserUtil.removeAllSpaces(FieldParserUtil.removeAllDoubleDot(FieldParserUtil.removeAllLetter(publication.getSP()))));
                publication.setSe(file.getSearchEngineType());
                if (publication.getU3()==null || publication.getU3().isEmpty())
                    publication.setUserDefined3(file.getDatabaseType()==DatabaseType.EMBASE?"Embase":(file.getDatabaseType()==DatabaseType.MEDLINE?"MedLine":"RefMan"));

                if (file.getDatabaseType()==DatabaseType.REFMAN)
                    if (publication.getJF()==null || publication.getJF().isEmpty())
                        if (publication.getJA()!=null && !publication.getJA().isEmpty())
                        {
                            publication.setJF(BL.Parser.MedLineJournalsParser.findFullJournalName(publication.getJA()));
                        }
                publications.add(publication);
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        for (RisPublication pub:publications)
            pub.setPubFile(file);
        file.setPublications(publications);
        return file;
    }

    private static List<List<PublicationElement>> extractPublicationElememts(RisPublicationFile file) throws FileNotFoundException, IOException
    {
        BufferedReader br = new BufferedReader(new FileReader(file.getAbsoluteFile()));
        List<List<PublicationElement>> studies= new ArrayList();
        List<PublicationElement> Publication=new ArrayList<PublicationElement>();
        PublicationElement currElement=new PublicationElement();
        try {
            String line;
            while ((line=br.readLine()) != null) {
                if (line.startsWith(new String('\ufeff' + "")) || line.equals(""))
                {
                    boolean containTag=false;
                    for (PublicationElement pE:Publication)
                            if (pE.Tag!=null) {containTag=true;}
                    if (containTag==true)
                        studies.add(Publication);
                    Publication=new ArrayList<PublicationElement>();
                }
                else if (isNewTag(line) && !(line.contains("IS -") && (line.contains("(Linking)")||line.contains("(Print)") || line.contains("(Electronic)"))))
                {
                    if (currElement!=null)
                        Publication.add(currElement);
                    currElement=new PublicationElement();
                    currElement.Tag=GetTag(line.substring(0,4));
                    currElement.Value=line.substring(6,line.length());
                }
                else if (line!="")
                {
                    currElement.Value+=" " + line.trim();
                }
            }
        } finally {
            boolean containTag=false;
            for (PublicationElement pE:Publication)
                if (pE.Tag!=null) {containTag=true;}
            if (containTag==true)
                studies.add(Publication);
            br.close();
        }
        return studies;
    }


    private static boolean isNewTag(String line)
    {
        for (int i=0;i<tags.length;i++)
            if (line.startsWith(tags[i]))
                return true;
        return false;
    }


    private static String GetTag(String line)
    {
        for (int i=0;i<tags.length;i++)
            if (line.startsWith(tags[i]))
                return tags[i];
        return "unknown";
    }
}
