package BL.Parser;

import BL.miner.Mineable;
import BO.Files.MedLinePublicationFile;
import BO.Publication.MedLinePublication;
import BO.Publication.Publication;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by casteven on 17/02/14.
 */
public class MedLinePublicationFileParser
{
    private static String[] tags=new String[]{"AB","CI","AD","IRAD","AID","AU","AUID","FAU","BTI","CTI","CN","CRDT",
                                              "DCOM","DA","LR","DEP","DP","EN","ED","FED","EDAT","GS","PG","PS","FPS",
                                              "PL","PHST","PST","PT","PUBM","PMC","PMCR","PMID","RN","NM","SI","SO","SFM",
                                              "STAT","SB","TI","TT","VI","VTI","GN","GR","IR FIR","ISBN","IS","IP","TA","JT",
                                              "LA","LID","MID","MHDA","MH","JID","RF","OABOABL","OCI","OID","OT","OTO","OWN"};

    public static MedLinePublicationFile ParseFile(MedLinePublicationFile file)
    {
        List<Mineable> publications=new ArrayList<Mineable>();
        try {
            for (List<PublicationElement> publicationElementList:extractPublicationElememts(file))
            {
                    Mineable publication=new Mineable();
                    for (PublicationElement element:publicationElementList)
                    {
                        if (element.Tag!=null)
                            {
                            if (element.Tag.equals("FAU"))
                                publication.addFAU(element.Value);
                            if (element.Tag.equals("AU"))
                                publication.addAU(element.Value);
                            if (element.Tag.equals("PL"))
                                publication.setPL(element.Value);
                            if (element.Tag.equals("PG"))
                                publication.setPG(element.Value);
                            if (element.Tag.equals("PMID"))
                            {
                                publication.setPMID(element.Value);
                                publication.setURL(element.Value);
                            }
                            if (element.Tag.equals("TA"))
                                publication.setTA(element.Value);
                            if (element.Tag.equals("JT"))
                                publication.setJT(element.Value);
                            if (element.Tag.equals("MH"))
                                publication.addMH(element.Value);
                            if (element.Tag.equals("JID"))
                                publication.setJID(element.Value);
                            if (element.Tag.equals("AB"))
                                publication.setAB(element.Value);
                            if (element.Tag.equals("AD"))
                                publication.setAD(element.Value);
                            if (element.Tag.equals("IS"))
                                publication.addISSN(element.Value);
                            if (element.Tag.equals("TI"))
                                publication.setTI(element.Value);
                            if (element.Tag.equals("PT"))
                                publication.addPT(element.Value);
                            if (element.Tag.equals("VI"))
                                publication.setVI(element.Value);
                            if (element.Tag.equals("DP"))
                                publication.setDP(element.Value);
                            if (element.Tag.equals("DA"))
                                publication.setDA(element.Value);
                            if (element.Tag.equals("LA"))
                                publication.setLA(element.Value);
                            if (element.Tag.equals("AID"))
                                publication.setAID(element.Value);
                        }
                    }
                publication.setSe(file.getSearchEngineType());
                publications.add(publication);
                }
            } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        for (MedLinePublication pub:publications)
            pub.setPubFile(file);
        file.setPublications(publications);
        return file;
    }

    private static List<List<PublicationElement>> extractPublicationElememts(MedLinePublicationFile file) throws FileNotFoundException, IOException
    {
        BufferedReader br = new BufferedReader(new FileReader(file.getAbsoluteFile()));
        List<List<PublicationElement>> studies= new ArrayList();
        List<PublicationElement> Publication=new ArrayList<PublicationElement>();
        PublicationElement currElement=new PublicationElement();
        try {
            String line;
            while ((line=br.readLine()) != null) {
                if (line.startsWith(new String('\ufeff' + "")) || line.equals(""))
                {
                    boolean containTag=false;
                    for (PublicationElement pE:Publication)
                        if (pE.Tag!=null) {containTag=true;}
                    if (containTag==true)
                        studies.add(Publication);
                    Publication=new ArrayList<PublicationElement>();
                }
                else if (isNewTag(line))
                {
                    if (currElement!=null)
                        Publication.add(currElement);
                    currElement=new PublicationElement();
                    currElement.Tag=GetTag(line.substring(0,4));
                    currElement.Value=line.substring(6,line.length());
                }
                else if (line!="")
                    currElement.Value+=" " + line.trim();
            }
        } finally {
            boolean containTag=false;
            for (PublicationElement pE:Publication)
                if (pE.Tag!=null) {containTag=true;}
            if (containTag==true)
                studies.add(Publication);
            br.close();
        }
        return studies;
    }


    private static boolean isNewTag(String line)
    {
       for (int i=0;i<tags.length;i++)
           if (line.startsWith(tags[i]))
               return true;
        return false;
    }


    private static String GetTag(String line)
    {
        for (int i=0;i<tags.length;i++)
            if (line.startsWith(tags[i]))
                return tags[i];
        return "unknown";
    }
}
