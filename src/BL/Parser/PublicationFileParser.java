package BL.Parser;

import BO.Files.ExcelPublicationFile;
import BO.Files.MedLinePublicationFile;
import BO.Files.RisPublicationFile;
import BO.Files.PublicationFile;
import BO.Publication.RisPublication;

import java.io.IOException;

/**
 * Created by casteven on 17/02/14.
 */
public class PublicationFileParser
{
    public static PublicationFile parse(PublicationFile f)
    {
        if (f.getClass().equals(MedLinePublicationFile.class))
        {
            f=MedLinePublicationFileParser.ParseFile((MedLinePublicationFile) f);
        }
        if (f.getClass().equals(RisPublicationFile.class))
        {
            f= RisPublicationFileParser.ParseFile((RisPublicationFile) f);
        }
        if (f.getClass().equals(ExcelPublicationFile.class))
        {
            try {
                ExcelPublicationFileParser parser=new ExcelPublicationFileParser();
                f=parser.ParseFile((ExcelPublicationFile) f);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return f;
    }
}
