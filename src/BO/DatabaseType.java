package BO;

/**
 * Created by casteven on 14/02/14.
 */
public enum DatabaseType
{
    MEDLINE,
    EMBASE,
    REFMAN,
    DEDUP,
    UNKNOWN
}
