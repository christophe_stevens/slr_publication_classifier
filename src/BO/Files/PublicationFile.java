package BO.Files;

import BL.miner.Mineable;
import BO.DatabaseType;
import BO.Publication.Publication;
import BO.Publication.RisPublication;
import BO.SearchEngineType;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by casteven on 17/02/14.
 */
public abstract class PublicationFile extends File
{
    private DatabaseType databaseType=DatabaseType.UNKNOWN;
    private SearchEngineType searchEngineType=SearchEngineType.Unknown_txt;
    protected List<Publication> publications=new ArrayList<Publication>();
    private DefaultTableModel myTableModel;
    private Double progression=0.0;
    private String status="";


    public PublicationFile(String pathname, TableModel TM) {
        super(pathname);
    }

    public PublicationFile(String parent, String child) {
        super(parent, child);
    }

    public PublicationFile(File parent, String child) {
        super(parent, child);
    }

    public PublicationFile(URI uri) {
        super(uri);
    }

    public Object[] toArray()
    {
        Object[] PublicationArray=new Object[3];
        PublicationArray[0]=this;
        PublicationArray[1]=databaseType;
        PublicationArray[2]=searchEngineType;
        return PublicationArray;
    }

     /*
    GETTER AND SETTES
     */

    public DatabaseType getDatabaseType() {
        return databaseType;
    }

    public void setDatabaseType(DatabaseType databaseType) {
        this.databaseType = databaseType;
    }

    public SearchEngineType getSearchEngineType() {
        return searchEngineType;
    }

    public void setSearchEngineType(SearchEngineType searchEngineType) {
        this.searchEngineType = searchEngineType;
    }

    protected void setRisPublicationList(List<RisPublication> RisPublications)
    {
        publications=new ArrayList<Publication>();
        for (Publication pb:RisPublications)
            publications.add(pb);
    }
    protected void setMedLinePublicationList(List<Mineable> MedLinePublications)
    {
        publications=new ArrayList<Publication>();
        for (Publication pb:MedLinePublications)
            publications.add(pb);
    }

    public List<Publication> getPublication()
    {
        return this.publications;
    }

    public Double getProgression() {
        return progression;
    }

    public void setProgression(Double progression) {
        this.progression = progression ;
        myTableModel.fireTableDataChanged();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
        myTableModel.fireTableDataChanged();
    }

    public TableModel getMyTableModel() {
        return myTableModel;
    }

    public void setMyTableModel(DefaultTableModel myTableModel) {
        this.myTableModel = myTableModel;
    }



}
