package BO.Files;

import BL.miner.Mineable;
import BO.DatabaseType;
import BO.SearchEngineType;

import javax.swing.table.DefaultTableModel;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by casteven on 17/02/14.
 */
public class MedLinePublicationFile extends PublicationFile
{
    private List<Mineable> publications=new ArrayList<Mineable>();

    public MedLinePublicationFile(String pathname, DefaultTableModel tableModel) {
        super(pathname, tableModel);
        super.setSearchEngineType(SearchEngineType.PubMed);
        super.setDatabaseType(DatabaseType.MEDLINE);
    }

    public MedLinePublicationFile(String parent, String child) {
        super(parent, child);
    }

    public MedLinePublicationFile(File parent, String child) {
        super(parent, child);
    }

    public MedLinePublicationFile(URI uri) {
        super(uri);
    }

    public List<Mineable> getPublications()
    {
        return publications;
    }

    public void setPublications(List<Mineable> publications)
    {
        this.publications=publications;
        super.setMedLinePublicationList(publications);
    }
}
