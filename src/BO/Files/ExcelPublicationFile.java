package BO.Files;

import BO.DatabaseType;
import BO.Publication.RisPublication;
import BO.SearchEngineType;

import javax.swing.table.DefaultTableModel;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by casteven on 19/03/14.
 */
public class ExcelPublicationFile extends PublicationFile
{
    private List<RisPublication> publications=new ArrayList<RisPublication>();

    public ExcelPublicationFile(String pathname, DatabaseType db, SearchEngineType se, DefaultTableModel tableModel) {
        super(pathname, tableModel);
        super.setDatabaseType(db);
        super.setSearchEngineType(se);
    }

    public ExcelPublicationFile(String parent, String child) {
        super(parent, child);
    }

    public ExcelPublicationFile(URI uri) {
        super(uri);
    }

    public ExcelPublicationFile(File parent, String child) {
        super(parent, child);
    }

    public List<RisPublication> getPublications()
    {
        return publications;
    }

    public void setPublications(List<RisPublication> publications)
    {
        this.publications=publications;
        super.setRisPublicationList(publications);
    }
}
