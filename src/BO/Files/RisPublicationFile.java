package BO.Files;

import BO.DatabaseType;
import BO.Publication.MedLinePublication;
import BO.Publication.Publication;
import BO.Publication.RisPublication;
import BO.SearchEngineType;

import javax.swing.table.DefaultTableModel;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by casteven on 17/02/14.
 */
public class RisPublicationFile extends PublicationFile
{
    private List<RisPublication> publications=new ArrayList<RisPublication>();

    public RisPublicationFile(String pathname, DatabaseType db, SearchEngineType se, DefaultTableModel tableModel) {
        super(pathname, tableModel);
        super.setDatabaseType(db);
        super.setSearchEngineType(se);
    }

    public RisPublicationFile(String parent, String child) {
        super(parent, child);
    }

    public RisPublicationFile(URI uri) {
        super(uri);
    }

    public RisPublicationFile(File parent, String child) {
        super(parent, child);
    }

    public List<RisPublication> getPublications()
    {
        return publications;
    }

    public void setPublications(List<RisPublication> publications)
    {
        this.publications=publications;
        super.setRisPublicationList(publications);
    }
}
