package BO.Publication;

import Util.FieldParserUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by casteven on 17/02/14.
 */
public class MedLinePublication extends Publication
{
    private String  PG;

    /*
    Authors
     */
    public List<String> getAU() {
        return super.getSecondaryAuthors();
    }
    public void addAU(String AU) {
        super.SecondaryAuthors.add(AU);
    }
    public List<String> getFAU() {
        return super.getPrimaryAuthors();
    }
    public void addFAU(String FAU) {
        super.addPrimaryAuthors(FAU);
    }


    /*
    Publication
     */
    public String getPL() {
        return super.getPlaceOfPublication();
    }

    public void setPL(String PL) {
        super.setPlaceOfPublication(PL);
    }

    /*
    PAGE NUMBER
     */
    public String getPG() {
        return PG;
    }

    public void setPG(String PG)
    {
        String[] pages= FieldParserUtil.ParsePageField(PG);
        super.setStartPageNumber(pages[0]);
        super.setEndPageNumber(pages[1]);
        this.PG = PG;
    }


    public String getPMID() {
        return super.getReferenceID();
    }

    public void setPMID(String PMID) {
        super.ReferenceID=PMID;
    }

    public List<String> getISSN() {
        return super.getISSN();
    }

    public void addISSN(String ISSN) {
        super.addISSN(ISSN);
    }

    public String getIP() {
        return super.getIssue();
    }

    public void setIP(String Issue) {
        super.setIssue(Issue);
    }

    public String getISBN() {
        return super.getISBN();
    }

    public void setISBN(String ISBN) {
        super.ISBN=ISBN;
    }

    public String getTA() {
        return super.getJournalAbbreviation();
    }

    public void setTA(String TA) {
        super.JournalAbbreviation=TA;
    }

    public String getJT() {
        return super.getJournalFull();
    }

    public void setJT(String JT) {
        super.JournalFull=JT;
    }

    public List<String> getMH() {
        return super.getKeywords();
    }

    public void addMH(String MH) {
        super.addKeywords(MH);
    }

    public void setURL(String URL)
    {
         super.setURL("PM:" + URL);
    }
    public String getURL()
    {
        return super.getURL();
    }



    /*
        Journal ID
     */
    public List<String> getJID() {
        return super.getMisc1();
    }
    public void setJID(String JID) {
        super.addMisc1(JID);
    }

    /*
        Abstract
     */
    public String getAB() {
        if (super.Abstract==null)
            return "";
       return super.Abstract;
    }

    public void setAB(String AB) {
        super.setAbstract(AB);
    }

    /*

     */
    public String getAD() {
        return super.getPublisher();
    }

    public void setAD(String AD) {
        super.setPublisher(AD);
    }

    public String getTI() {
        if (super.getTitle()==null)
            return "";
        return super.getTitle();
    }

    public void setTI(String TI) {
        super.setTitle(TI);
    }

    public List<String> getPT() {
        return super.getTypeOfRecord();
    }

    public void addPT(String PT) {
        super.addTypeOfRecord(PT);
    }

    public String getVI() {
        return super.getVolumeNumber();
    }

    public void setVI(String VI) {
        super.setVolumeNumber(VI);
    }

    public String getDP() {
        return super.getPrimaryDate();
    }

    public void setDP(String DP) {
        super.setPrimaryDate(DP);
    }

    public String getDA() {
        return super.getPublicationYear();
    }

    public void setDA(String DA) {
        super.setPublicationYear(DA);
    }

    public void setLA(String LA)
    {
        super.setLanguage(LA);
    }

    public String getLA()
    {
        return super.getLanguage();
    }
    public void setAID(String AID)
    {
        super.setUniqueReferenceNumber(AID);
    }

    public String getAID()
    {
        return super.getUniqueReferenceNumber();
    }

    public void setBTI(String BTI)
    {
        super.setBookTitle(BTI);
    }

    public String getBTI()
    {
        return super.getBookTitle();
    }


}
