package BO.Publication;

import java.util.List;


public class RisPublication extends Publication
{
    public RisPublication()
    {
    }

    public List<String> getA1() {
        return super.getPrimaryAuthors();
    }
    public void addA1(String a1) {
        super.addPrimaryAuthors(a1);
    }

    public String getAD() {
        return super.getAdress();
    }

    public void setAD(String AD) {
       super.setAdress(AD);
    }

    public String getCY() {
        return super.getPlaceOfPublication();
    }

    public void setCY(String CY) {
        super.setPlaceOfPublication(CY);
    }

    public String getEP() {
        return super.getEndPageNumber();
    }

    public void setEP(String EP) {
        super.setEndPageNumber(EP);
    }

    public String getID() {
        return super.getReferenceID();
    }

    public void setID(String ID) {
        super.setReferenceID(ID);
    }

    public String getIS() {
       return super.getIssue();
    }

    public void setIS(String IS) {
        super.setIssue(IS);
    }

    public String getJA() {
        return super.getJournalAbbreviation();
    }

    public void setJA(String JA) {
        super.setJournalAbbreviation(JA);
    }

    public String getJF() {
        return super.getJournalFull();
    }

    public void setJF(String JF) {
        super.setJournalFull(JF);
    }

    public List<String> getKW() {
        return super.getKeywords();
    }

    public void addKW(String KW) {
        super.addKeywords(KW);
    }

    public String getL1() {
        return super.getLinkPDF();
    }

    public void setL1(String l1) {
        super.setLinkPDF(l1);
    }

    public String getL2() {
        return super.getLinkFullText();
    }

    public void setL2(String l2) {
        super.setLinkFullText(l2);
    }

    public String getL3() {
        return  super.getLinkRelatedRecord();
    }

    public void setL3(String l3) {
        super.setLinkRelatedRecord(l3);
    }

    public String getL4() {
        return super.getLinkImages();
    }

    public void setL4(String l4) {
        super.setLinkImages(l4);
    }

    public List<String> getM1() {
        return super.getMisc1();
    }

    public void setM1(String m1) {
        super.addMisc1(m1);
    }

    public String getM3() {
        return super.getMisc3();
    }

    public void setM3(String m3) {
        super.setMisc3(m3);
    }

    public String getN2() {
        return super.getAbstract();
    }

    public void setN2(String n2) {
        super.setAbstract(n2);
    }

    public String getPB() {
        return super.getPublisher();
    }

    public void setPB(String PB) {
        super.setPublisher(PB);
    }

    public String getSN() {
        return super.getSN();
    }

    public void setSN(String SN) {
        super.setSN(SN);
    }

    public String getSP() {
        return super.getStartPageNumber();
    }

    public void setSP(String SP) {
        super.setStartPageNumber(SP);
    }

    public String getT1() {
        return super.getTitle();
    }

    public void setT1(String t1) {
        super.setTitle(t1);
    }

    public String getTI() {
        return super.getBookTitle();
    }

    public void setTI(String TI) {
        super.setBookTitle(TI);
    }

    public List<String> getTY() {
        return super.getTypeOfRecord();
    }

    public void setTY(String TY) {
        super.addTypeOfRecord(TY);
    }

    public String getUR() {
        return super.getURL();
    }

    public void setUR(String UR) {
        super.setURL(UR);
    }

    public String getVL() {
        return super.getVolumeNumber();
    }

    public void setVL(String VL) {
        super.setVolumeNumber(VL);
    }

    public String getY1() {
        return super.getPrimaryDate();
    }

    public void setY1(String y1) {
        super.setPrimaryDate(y1);
    }

    public List<String> getAU() {
        return super.getAuthors();
    }

    public void setAU(String AU) {
        super.addAuthors(AU);
    }

    public List<String> getA2() {
        return super.getSecondaryAuthors();
    }

    public void setA2(String a2) {
        super.addSecondaryAuthors(a2);
    }

    public String getPY() {
        return super.getPublicationYear();
    }

    public void setPY(String PY) {
        super.setPublicationYear(PY);
    }

    public String getCT() {
        return super.getUnpublishedPublicationTitle();
    }

    public void setCT(String CT) {
        super.setUnpublishedPublicationTitle(CT);
    }

    public String getN1() {
        return super.getNotes();
    }

    public void setN1(String n1) {
        super.setNotes(n1);
    }

    public String getRP() {
        return super.getReprintStatus();
    }

    public void setRP(String RP) {
        super.setReprintStatus(RP);
    }

    public String getJO() {
        return super.getPeriodicalStandardAbbr();
    }

    public void setJO(String JO) {
        super.setPeriodicalStandardAbbr(JO);
    }

    public String getJ1() {
        return super.getPeriodicalNameUserAbbr1();
    }

    public void setJ1(String j1) {
        super.setPeriodicalStandardAbbr(j1);
    }

    public String getJ2() {
        return super.getPeriodicalNameUserAbbr2();
    }

    public void setJ2(String j2) {
        super.setPeriodicalNameUserAbbr2(j2);
    }

    public String getT2() {
        return super.getSecondaryTitle();
    }

    public void setT2(String t2) {
        super.setSecondaryTitle(t2);
    }

    public String getT3() {
        return super.getTitleSeries();
    }

    public void setT3(String t3) {
        super.setTitleSeries(t3);
    }

    public String getAV() {
        return super.getAvailability();
    }

    public void setAV(String AV) {
        super.setAvailability(AV);
    }

    public String getU1() {
        return super.getUserDefined1();
    }

    public void setU1(String u1) {
        super.setUserDefined1(u1);
    }

    public String getU2() {
        return super.getUserDefined2();
    }

    public void setU2(String u2) {
        super.setUserDefined2(u2);
    }

    public String getU3() {
        return super.getUserDefined3();
    }

    public void setU3(String u3) {
        super.setUserDefined3(u3);
    }

    public String getU4() {
        return super.getUserDefined4();
    }

    public void setU4(String u4) {
        super.setUserDefined4(u4);
    }

    public String getU5() {
        return super.getUserDefined5();
    }

    public void setU5(String u5) {
        super.setUserDefined5(u5);
    }

    public void setDO(String AID)
    {
        super.setUniqueReferenceNumber(AID);
    }

    public String getDO()
    {
        return super.getUniqueReferenceNumber();
    }
}
