package BO.Publication;

import BO.Files.PublicationFile;
import BO.SearchEngineType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by casteven on 18/02/14.
 */
public abstract class Publication
{
    private SearchEngineType se;

    private PublicationFile PubFile;

    //Location
    protected String Adress;
    protected String PlaceOfprotectedation;
    //References and ID
    protected List<String> TypeOfRecord=new ArrayList<String>();
    protected String ReferenceID;

    protected String UniqueReferenceNumber;
    protected String SN;
    protected List<String> ISSN=new ArrayList<String>(); //List Electronic or not
    protected String ISBN;

    protected String Issue;
    protected String VolumeNumber;
    protected String StartPageNumber;
    protected String EndPageNumber;
    //Journal
    protected String JournalAbbreviation;
    protected String JournalFull;
    protected String ReprintStatus;
    protected String PeriodicalStandardAbbr;
    protected String PeriodicalNameUserAbbr1;
    protected String PeriodicalNameUserAbbr2;
    protected String Availability;
    //Author
    protected List<String> Authors=new ArrayList<String>();
    protected List<String> PrimaryAuthors=new ArrayList<String>();
    protected List<String> SecondaryAuthors=new ArrayList<String>();
    //Search
    protected List<String> Keywords=new ArrayList<String>();
    protected String Language;
    //Links
    protected String LinkPDF;
    protected String LinkFullText;
    protected String LinkRelatedRecord;
    protected String LinkImages;
    protected String URL;
    //Miscelaeneous
    protected List<String> Misc1=new ArrayList<String>();
    protected String Misc3;
    protected String Notes;
    //Content
    protected String Abstract;
    protected String Publisher;
    protected String Title;
    protected String SecondaryTitle;
    protected String TitleSeries;
    protected String UnpublishedPublicationTitle;
    protected String BookTitle;
    protected String PublicationYear;
    protected String PrimaryDate;
    //UF
    protected String UserDefined1;
    protected String UserDefined2;
    protected String UserDefined3;
    protected String UserDefined4;
    protected String UserDefined5;

    protected Publication()
    {};

    protected String getAdress() {
        return Adress;
    }

    protected void setAdress(String adress) {
        Adress = adress;
    }

    protected String getPlaceOfPublication() {
        return PlaceOfprotectedation;
    }

    protected void setPlaceOfPublication(String placeOfprotectedation) {
        PlaceOfprotectedation = placeOfprotectedation;
    }

    protected List<String> getTypeOfRecord() {
        return TypeOfRecord;
    }

    protected void addTypeOfRecord(String typeOfRecord) {
        TypeOfRecord.add(typeOfRecord);
    }

    protected String getReferenceID() {
        return ReferenceID;
    }

    protected void setReferenceID(String referenceID) {
        ReferenceID = referenceID;
    }

    protected List<String> getISSN() {
        return ISSN;
    }

    protected void addISSN(String ISSN) {
        this.ISSN.add(ISSN);
    }

    protected String getISBN() {
        return ISBN;
    }

    protected void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    protected String getVolumeNumber() {
        return VolumeNumber;
    }

    protected void setVolumeNumber(String volumeNumber) {
        VolumeNumber = volumeNumber;
    }

    public String getStartPageNumber() {
        return StartPageNumber;
    }

    protected void setStartPageNumber(String startPageNumber) {
        StartPageNumber = startPageNumber;
    }

    public String getEndPageNumber() {
        return EndPageNumber;
    }

    protected void setEndPageNumber(String endPageNumber) {
        EndPageNumber = endPageNumber;
    }

    protected String getJournalAbbreviation() {
        return JournalAbbreviation;
    }

    protected void setJournalAbbreviation(String journalAbbreviation) {
        JournalAbbreviation = journalAbbreviation;
    }

    protected String getJournalFull() {
        return JournalFull;
    }

    protected void setJournalFull(String journalFull) {
        JournalFull = journalFull;
    }

    protected String getReprintStatus() {
        return ReprintStatus;
    }

    protected void setReprintStatus(String reprintStatus) {
        ReprintStatus = reprintStatus;
    }

    protected String getPeriodicalStandardAbbr() {
        return PeriodicalStandardAbbr;
    }

    protected void setPeriodicalStandardAbbr(String periodicalStandardAbbr) {
        PeriodicalStandardAbbr = periodicalStandardAbbr;
    }

    protected  String getPeriodicalNameUserAbbr1() {
        return PeriodicalNameUserAbbr1;
    }

    protected  void setPeriodicalNameUserAbbr1(String periodicalNameUserAbbr1) {
        PeriodicalNameUserAbbr1 = periodicalNameUserAbbr1;
    }

    protected  String getPeriodicalNameUserAbbr2() {
        return PeriodicalNameUserAbbr2;
    }

    protected  void setPeriodicalNameUserAbbr2(String periodicalNameUserAbbr2) {
        PeriodicalNameUserAbbr2 = periodicalNameUserAbbr2;
    }

    protected String getAvailability() {
        return Availability;
    }

    protected void setAvailability(String availability) {
        Availability = availability;
    }

    protected  List<String> getAuthors() {
        return Authors;
    }

    protected void addAuthors(String authors) {
        Authors.add(authors);
    }

    protected List<String> getPrimaryAuthors() {
        return PrimaryAuthors;
    }

    protected void addPrimaryAuthors(String primaryAuthor) {
        PrimaryAuthors.add(primaryAuthor);
    }

    protected List<String> getSecondaryAuthors() {
        return SecondaryAuthors;
    }

    protected void addSecondaryAuthors(String secondaryAuthor) {
        SecondaryAuthors.add(secondaryAuthor);
    }

    protected List<String> getKeywords() {
        return Keywords;
    }

    protected void addKeywords(String keyword) {
        Keywords.add(keyword);
    }

    protected String getLinkPDF() {
        return LinkPDF;
    }

    protected void setLinkPDF(String linkPDF) {
        LinkPDF = linkPDF;
    }

    protected String getLinkFullText() {
        return LinkFullText;
    }

    protected void setLinkFullText(String linkFullText) {
        LinkFullText = linkFullText;
    }

    protected String getLinkRelatedRecord() {
        return LinkRelatedRecord;
    }

    protected void setLinkRelatedRecord(String linkRelatedRecord) {
        LinkRelatedRecord = linkRelatedRecord;
    }

    protected String getLinkImages() {
        return LinkImages;
    }

    protected void setLinkImages(String linkImages) {
        LinkImages = linkImages;
    }

    protected String getURL() {
        return URL;
    }

    protected void setURL(String URL)
    {
        if (this.URL==null)
            this.URL=URL;
        else if (!this.URL.contains("PM:"))
            this.URL = URL;
    }

    protected List<String> getMisc1() {
        return Misc1;
    }

    protected void addMisc1(String misc1) {
        Misc1.add(misc1);
    }

    protected String getMisc3() {
        return Misc3;
    }

    protected void setMisc3(String misc3) {
        Misc3 = misc3;
    }

    protected String getNotes() {
        return Notes;
    }

    protected void setNotes(String notes) {
        Notes = notes;
    }

    protected String getAbstract() {
        return Abstract;
    }

    protected void setAbstract(String anAbstract) {
        Abstract = anAbstract;
    }

    protected String getPublisher() {
        return Publisher;
    }

    protected void setPublisher(String publisher) {
        Publisher = publisher;
    }

    protected String getTitle() {
        return Title;
    }

    protected void setTitle(String title) {
        Title = title;
    }

    protected String getSecondaryTitle() {
        return SecondaryTitle;
    }

    protected void setSecondaryTitle(String secondaryTitle) {
        SecondaryTitle = secondaryTitle;
    }

    protected String getTitleSeries() {
        return TitleSeries;
    }

    protected void setTitleSeries(String titleSeries) {
        TitleSeries = titleSeries;
    }

    protected String getUnpublishedPublicationTitle() {
        return UnpublishedPublicationTitle;
    }

    protected void setUnpublishedPublicationTitle(String unpublishedprotectedationTitle) {
        UnpublishedPublicationTitle = unpublishedprotectedationTitle;
    }

    protected String getBookTitle() {
        return BookTitle;
    }

    protected void setBookTitle(String bookTitle) {
        BookTitle = bookTitle;
    }

    protected String getPublicationYear() {
        return PublicationYear;
    }

    protected void setPublicationYear(String protectedationYear) {
        PublicationYear = protectedationYear;
    }

    protected String getPrimaryDate() {
        return PrimaryDate;
    }

    protected void setPrimaryDate(String primaryDate) {
        PrimaryDate=primaryDate;
    }

    public String getUserDefined1() {
        return UserDefined1;
    }

    public void setUserDefined1(String userDefined1) {
        UserDefined1 = userDefined1;
    }

    public String getUserDefined2() {
        return UserDefined2;
    }

    public void setUserDefined2(String userDefined2) {
        UserDefined2 = userDefined2;
    }

    public String getUserDefined3() {
        return UserDefined3;
    }

    public void setUserDefined3(String userDefined3) {
        UserDefined3 = userDefined3;
    }

    public String getUserDefined4() {
        return UserDefined4==null?"":UserDefined4;
    }

    public void setUserDefined4(String userDefined4) {
        UserDefined4 = userDefined4;
    }

    protected String getUserDefined5() {
        return UserDefined5;
    }

    protected void setUserDefined5(String userDefined5) {
        UserDefined5 = userDefined5;
    }

    public SearchEngineType getSe() {
        return se;
    }

    public void setSe(SearchEngineType se) {
        this.se = se;
    }

    public String getIssue() {
        return Issue;
    }

    public void setIssue(String issue) {
        Issue = issue;
    }

    protected String getLanguage() {
        return Language;
    }

    protected void setLanguage(String language) {
        Language = language;
    }

    protected String getUniqueReferenceNumber() {
        return UniqueReferenceNumber;
    }

    protected void setUniqueReferenceNumber(String uniqueReferenceNumber) {
        UniqueReferenceNumber = uniqueReferenceNumber;
    }

    public String getSN() {
        return SN;
    }

    public void setSN(String SN) {
        this.SN = SN;
    }

    public PublicationFile getPubFile() {
        return PubFile;
    }

    public void setPubFile(PublicationFile pubFile) {
        PubFile = pubFile;
    }



    @Override
    public String toString()
    {
        return this.getSe() + " " + this.getUserDefined3() + " " +  this.getUserDefined4() +  " " + this.Title + " " +  this.getJournalAbbreviation() + " " +  this.getPrimaryAuthors().toString();
    }

}
