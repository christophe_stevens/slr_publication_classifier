package BO.Journal;

/**
 * Created by casteven on 26/02/14.
 */
public class MedLineJournal
{
    private String JrId, JournalTitle, MedAbbr, ISSNp,ISSNo,IsoAbbr,NlmId;

    public String getJrId() {
        return JrId;
    }

    public void setJrId(String jrId) {
        JrId = jrId;
    }

    public String getJournalTitle() {
        return JournalTitle;
    }

    public void setJournalTitle(String journalTitle) {
        JournalTitle = journalTitle;
    }

    public String getMedAbbr() {
        return MedAbbr;
    }

    public void setMedAbbr(String medAbbr) {
        MedAbbr = medAbbr;
    }

    public String getISSNp() {
        return ISSNp;
    }

    public void setISSNp(String ISSNp) {
        this.ISSNp = ISSNp;
    }

    public String getISSNo() {
        return ISSNo;
    }

    public void setISSNo(String ISSNo) {
        this.ISSNo = ISSNo;
    }

    public String getIsoAbbr() {
        return IsoAbbr;
    }

    public void setIsoAbbr(String isoAbbr) {
        IsoAbbr = isoAbbr;
    }

    public String getNlmId() {
        return NlmId;
    }

    public void setNlmId(String nlmId) {
        NlmId = nlmId;
    }
}
