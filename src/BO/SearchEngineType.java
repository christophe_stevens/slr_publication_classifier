package BO;

/**
 * Created by casteven on 17/02/14.
 */
public enum SearchEngineType
{
    OvidSp,
    PubMed,
    RefMan,
    Unknown_xlsx,
    Unknown_txt
}
