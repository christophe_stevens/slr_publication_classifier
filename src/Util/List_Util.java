package Util;

import org.apache.poi.util.ArrayUtil;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: casteven
 * Date: 12/11/13
 * Time: 14:01
 * To change this template use File | Settings | File Templates.
 */
public class List_Util {

    public static String convertToCommaSeparatedList(List<Object> list) {
        String res = "";
        for (Iterator<Object> iterator = list.iterator(); iterator.hasNext();) {
            res += iterator.next().toString() + (iterator.hasNext() ? "," : "");
        }
        return res;
    }
    public static String convertToCommaSeparatedStringList(List<String> list) {
        String res = "";
        for (Iterator<String> iterator = list.iterator(); iterator.hasNext();) {
            res += iterator.next().toString() + (iterator.hasNext() ? "," : "");
        }
        return res;
    }
    public static List<String> convertCommaSeparatedStringToListOfString(String word) {
        String res = "";
        String[] arrayOfString=word.split(",");
        List<String> listOfString= Arrays.asList(arrayOfString);
        return listOfString;
    }
}
