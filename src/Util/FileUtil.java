package Util;

import java.io.File;

/**
 * Created by casteven on 19/02/14.
 */
public class FileUtil
{
    public static String DefaultLocation="C:\\Temp\\DuplicateFinder\\RefMan";

    public final static String ris= "ris";
    public final static String txt = "txt";
    public final static String xlsx = "xlsx";
    public final static String xls = "xls";

        /*
         * Get the extension of a file.
         */
        public static String getExtension(File f) {
            String ext = null;
            String s = f.getName();
            int i = s.lastIndexOf('.');

            if (i > 0 &&  i < s.length() - 1) {
                ext = s.substring(i+1).toLowerCase();
            }
            return ext;
        }

        public static void  OpenFile(String filename)
        {
            try {
                if (filename.startsWith("\\\\"))
                    OpenNetworkFile(filename);
                else
                    OpenLocalFile(filename);
            }catch (Exception e)
            {
            }
        }

        private static void OpenLocalFile(String filename)throws Exception
        {
            System.out.println((String.format("cmd /c \"%s\"",filename.replaceAll("\\(","^(").replaceAll("\\)","^)").replaceAll(" ","^ "))));
            Runtime.getRuntime().exec(String.format("cmd /c \"%s\"",filename.replaceAll("\\(","^(").replaceAll("\\)","^)").replaceAll(" ","^ ")));
        }
        private static void OpenNetworkFile(String filename)throws Exception
        {
            System.out.println(String.format("cmd /c \"%s\"",filename));
            Runtime.getRuntime().exec(String.format("cmd /c \"%s\"",filename));
        }
}
