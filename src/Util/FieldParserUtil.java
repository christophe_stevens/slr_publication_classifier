package Util;

/**
 * Created by casteven on 19/02/14.
 */
public class FieldParserUtil
{
    public static String[] ParsePageField(String PageField)
    {

        String clearedField=removeAllDoubleDot(removeAllSpaces(PageField));
        if (clearedField.contains(";"))
            clearedField=clearedField.split(";")[0];
        if (clearedField.contains("e"))
        clearedField=clearedField.substring(clearedField.indexOf("e"),clearedField.length());
        clearedField=removeAllLetter( clearedField);
        String sp="";
        String ep="";
        if (clearedField.contains("-") & !clearedField.replace("-","").isEmpty())
        {
            try {
                //if 2 pages are showed
                sp = clearedField.split("-")[0];
                ep = clearedField.split("-")[1];
            }catch(Exception e){
                System.out.println("");
            }
        }
        else
        {
            //if only one pages are showed
            if (clearedField.contains(","))
            {
                //If pages with delimiter
                int firstIndexOfCom=clearedField.indexOf(",");
                int lastIndexOfCom=clearedField.lastIndexOf(",");
                sp=clearedField.substring(0,firstIndexOfCom);
                ep=clearedField.substring(lastIndexOfCom,clearedField.length());
            }
            else
            {
                sp=clearedField;
                ep="";
            }
        }
        ep=BuildSecondPageOnMedLineNotation(sp, ep);
        String[] toReturn= new String[]{sp.trim(),ep.trim()};
        return toReturn;
    }

    /*
        MedLine give the page as
        SP=240
        EP=6
        It means from page 240 to 246. Therefor we need to Build it
     */
    public static String BuildSecondPageOnMedLineNotation(String sp, String ep)
    {
        if (sp==null || ep==null)
            return null;
        //CreateSecondPage
        int startToRemove;
        if(!ep.isEmpty() && (startToRemove = sp.length() - ep.length())>0)
        {
            ep=sp.substring(0,startToRemove)+""+ep;
        }
        return ep;
    }


    public static String removeAllLetter(String word)
    {
        if (word==null)
            return null;
        String WithoutLetterChar="";
        for (char c:word.toCharArray())
        {
            if (!((((int)c)>=65 && ((int)c)<=90) || (((int)c)>=97 && ((int)c)<=122)))
                WithoutLetterChar+=c;
        }
        return WithoutLetterChar;
    }
    public static String removeAllSpaces(String word)
    {
        if (word==null)
            return null;
        String WithoutLetterChar="";
        for (char c:word.toCharArray())
        {
            if (((int)c)!=32)
                WithoutLetterChar+=c;
        }
        return WithoutLetterChar;
    }
    public static String removeAllDoubleDot(String word)
    {
        if (word==null)
            return null;
        String WithoutLetterChar="";
        for (char c:word.toCharArray())
        {
            if (((int)c)!=58)
                WithoutLetterChar+=c;
        }
        return WithoutLetterChar;
    }

    public static String removeAllDot(String word)
    {
        if (word==null)
            return null;
        String WithoutLetterChar="";
        for (char c:word.toCharArray())
        {
            if (((int)c)!=46)
                WithoutLetterChar+=c;
        }
        return WithoutLetterChar;
    }
    public static String removeAllBracket(String word)
    {
        if (word==null)
            return null;
        String WithoutLetterChar="";
        for (char c:word.toCharArray())
        {
            if (!(((int)c)==91 || ((int)c)==93))
                WithoutLetterChar+=c;
        }
        return WithoutLetterChar;
    }
}
