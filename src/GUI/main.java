package GUI;

import BL.Writer.PublicationRisFileWriter;
import BL.classifier.Classifier;
import BL.miner.Mineable;
import BO.Files.MedLinePublicationFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class main
{
    private static String PATH="R:\\Christophe\\SLR_searches\\Skin\\CountSkinUpdate\\EndNote\\";
    private static File FILE=new File(PATH + "pubmed_file.txt");

    public static void main(String [] args)
    {
        MedLinePublicationFile mpubfile= BL.Parser.MedLinePublicationFileParser.ParseFile(new MedLinePublicationFile(FILE.toURI())) ;
        List<Mineable> publicationsInFile=mpubfile.getPublications();
        List<String> dictionaries=new ArrayList<String>();

        dictionaries.add("cancer_clinical");
        dictionaries.add("cancer_epidemiological");
        dictionaries.add("cancer_human");
        dictionaries.add("cancer_nonepidemiological");

        /**
         * Counter for displaying process
         */
        double total_size= publicationsInFile.size();
        double current=0;

        /**
         * Actual mingin process
         */
        for (Mineable pub:publicationsInFile) {
            pub.mine(dictionaries);
            current++;
            System.out.println((current/total_size)*100);
        }
        /**
         * Export in a Ris file
         */
        PublicationRisFileWriter.writeFile(publicationsInFile, new File(PATH + "selected_from_medline.ris"), Classifier.Classifiers.COMBINED);

        /**
         *  Export Results in an Excel file
         */
        PublicationRisFileWriter.writeFile(publicationsInFile, new File(PATH + "selected_from_medline.csv"), Classifier.Classifiers.COMBINED);
    }
}
